#SM_contest

А наш конкурс набирає обертів,  і ось Вам завдання з коефіцієнтом важкості  0.9!
Уявімо певну країну,  де не оптимальним способом прокладено дороги між містами. За планом всі дороги односторонні, але між двома містами може бути більше однієї дороги, можливо, в різних напрямках.
Певна логістична компанія планує перевозити вантажі в такій країні. 
Логістична компанія називає такий план доріг «слабко k-зв’язаним», якщо виконана наступна умова: для будь-яких двох різних міст можна проїхати від одного до іншого, порушуючи правила руху не більше k разів. Порушення правил – це проїзд по існуючій дорозі в зворотньому напрямку. Гарантується, що між будь-якими двома містами можна проїхати, можливо, кілька раз порушивши правила.
Уявімо, що Ви- аналітик логістичної компанії. Вам потрібно оцінити – яку мінімальну кількість порушень доведеться здійснити при перевозці вантажів із будь-якого міста в будь-яке.
Складіть програмний код, який буде рахувати мінімальну кількість порушень для будь-якого плану доріг, що вводиться.
Вхідні данні
У першому рядку вхідного файла (чи консолі) дано два числа 2 ≤ n ≤ 100 и 1 ≤ m ≤ 200 - кількість міст та доріг в плані. В наступних m рядках дано по два числа - номера міст, в яких починається та закінчується відповідна дорога.
Вихідні данні
В вихідний файл (чи консоль) виведіть мінімальне k, таке, що даний у вхідному файлі план є слабко k-зв’язаним.

And our competition is gaining momentum, and here's a task with a difficulty rate of 0.9!
Imagine a country where roads between cities are not made in an optimal way. According to the plan, all roads are one-way, but between the two cities, there may be more than one road, possibly in different directions.
A certain logistics company plans to transport goods in such a country.
A logistics company calls such a road plan "weakly k-linked" if the following condition is met: for any two different cities, you can drive from one to the other, violating traffic rules no more than k-times. Violation of the rules is a drive on the existing road in the opposite direction.
It is guaranteed that you can drive between any two cities, possibly breaking the rules several times.
Imagine that you are an analyst at this logistics company. You need to estimate - what is the minimum number of violations will have to be committed when transporting goods from any city to any.
Write a program code that will count the minimum number of violations for any road plan entered.
Input data
The first line of the input file (or console) gives two numbers 2 ≤ n ≤ 100 and 1 ≤ m ≤ 200 - the number of cities and roads in the plan. The next m lines give two numbers - the numbers of cities where the road begins and ends.
Initial data
Print a minimum k in the output file (or console), such that the plan in the input file is weakly k-linked.

unit solver;

interface

const
  DIJKSTRA = 'Dijkstra';
  ALPHA_BETA = 'AlphaBeta';
  FLOYD = 'Floyd';

const
  BIG = 10000;

type
  t_roads = array of array [0 .. 1] of integer;

function Solv(p_cityCnt: integer; p_roadCnt: integer; p_roads: t_roads; p_algorithm: string): integer;

implementation

uses
  SysUtils, Generics.Collections;

type
  t_connections = array of array of integer;

function SolvDijkstra(p_cityCnt: integer; const p_RConn: t_connections; const p_VConn: t_connections): integer; forward;
function SolvDijkstraOne(p_cityCnt, p_city: integer; const p_RConn: t_connections; const p_VConn: t_connections): integer; forward;
function SolvAlphaBeta(p_cityCnt: integer; p_RConn: t_connections; p_VConn: t_connections): integer; forward;
function SolvFloyd(p_cityCnt: integer; p_roadCnt: integer; p_roads: t_roads): integer; forward;

function Solv(p_cityCnt: integer; p_roadCnt: integer; p_roads: t_roads; p_algorithm: string): integer;
var
  wI, wFrom, wTo: integer;
  wRCnt, wVCnt: array of integer; // count of right and with violation roads
  wRConnection, wVConnection: t_connections; // right and with violation connections between cityes
begin
  if p_algorithm = FLOYD then begin
    result := SolvFloyd(p_cityCnt, p_roadCnt, p_roads);
    exit;
  end;

  SetLength(wRCnt, p_cityCnt);
  SetLength(wVCnt, p_cityCnt);
  for wI := 0 to p_roadCnt - 1 do begin
    inc(wRCnt[p_roads[wI][0] - 1]);
    inc(wVCnt[p_roads[wI][1] - 1]);
  end;

  SetLength(wRConnection, p_cityCnt);
  SetLength(wVConnection, p_cityCnt);
  for wI := 0 to p_cityCnt - 1 do begin
    SetLength(wRConnection[wI], wRCnt[wI]);
    SetLength(wVConnection[wI], wVCnt[wI]);
    dec(wRCnt[wI]);
    dec(wVCnt[wI]);
  end;

  for wI := 0 to p_roadCnt - 1 do begin
    wFrom := p_roads[wI][0] - 1;
    wTo := p_roads[wI][1] - 1;
    wRConnection[wFrom, wRCnt[wFrom]] := wTo;
    wVConnection[wTo, wVCnt[wTo]] := wFrom;
    dec(wRCnt[wFrom]);
    dec(wVCnt[wTo]);
  end;

  if p_algorithm = DIJKSTRA then
    result := SolvDijkstra(p_cityCnt, wRConnection, wVConnection)
  else if p_algorithm = ALPHA_BETA then
    result := SolvAlphaBeta(p_cityCnt, wRConnection, wVConnection)
  else
    raise Exception.Create('Unknown algorithm');
end;

function SolvDijkstra(p_cityCnt: integer; const p_RConn: t_connections; const p_VConn: t_connections): integer;
var
  wI, localViolation: integer;
begin
  result := 0;
  for wI := 0 to p_cityCnt - 1 do begin
    localViolation := SolvDijkstraOne(p_cityCnt, wI, p_RConn, p_VConn);
    if result < localViolation then
    begin
      result := localViolation;
    end;
  end;
end;

function SolvDijkstraOne(p_cityCnt: integer; p_city: integer; const p_RConn: t_connections; const p_VConn: t_connections): integer;
var
  wViolation: array of integer;
  wVisited: array of boolean;
  wI, wCurCity, wConnectedCity, wCurrViolation: integer;
  hasNotVisited, hasChanged: boolean;
begin
  SetLength(wViolation, p_cityCnt);
  SetLength(wVisited, p_cityCnt);
  for wI := 0 to p_cityCnt - 1 do begin
    wViolation[wI] := BIG;
  end;
  wCurCity := p_city;
  wCurrViolation := 0;
  wViolation[wCurCity] := 0;
  hasNotVisited := false;
  hasChanged := false;

  repeat
    if not wVisited[wCurCity] then begin
      hasNotVisited := true;
      if wViolation[wCurCity] = wCurrViolation then begin
        for wI := 0 to length(p_RConn[wCurCity]) - 1 do begin
          wConnectedCity := p_RConn[wCurCity][wI];
          if not wVisited[wConnectedCity] and (wViolation[wConnectedCity] > wCurrViolation) then
            wViolation[wConnectedCity] := wCurrViolation;
        end;
        for wI := 0 to length(p_VConn[wCurCity]) - 1 do begin
          wConnectedCity := p_VConn[wCurCity][wI];
          if not wVisited[wConnectedCity] and (wViolation[wConnectedCity] > wCurrViolation + 1) then
            wViolation[wConnectedCity] := wCurrViolation + 1;
        end;
        wVisited[wCurCity] := true;
        hasChanged := true;
      end;
    end;
    inc(wCurCity);
    if wCurCity >= p_cityCnt then
    begin
      if not hasNotVisited then
        break;
      if not hasChanged then
        inc(wCurrViolation);
      wCurCity := 0;
      hasNotVisited := false;
      hasChanged := false;
    end;
  until false;

  result := wCurrViolation;
end;

type

  t_CityDetail = record
    minEstimation, maxEstimation, violationsFrom, violationsTo: integer;
  end;

procedure SolvAlphaBetaOne(var p_AllCity: array of t_CityDetail; p_city: integer; p_RConn: t_connections; p_VConn: t_connections);
var
  wI, wCityCnt, wCurCity, wConnectedCity, wCurrViolation: integer;
  wSCurr, wSNext, wSTemp: TStack<integer>;
begin
  wSCurr := TStack<integer>.Create;
  wSNext := TStack<integer>.Create;
  wCityCnt := length(p_AllCity);

  for wI := 0 to wCityCnt do begin
    p_AllCity[wI].violationsFrom := BIG;
    p_AllCity[wI].violationsTo := BIG;
  end;
  p_AllCity[p_city].violationsFrom := 0;
  p_AllCity[p_city].violationsTo := 0;
  wCurrViolation := 0;

  wSCurr.Push(p_city);
  repeat
    if wSCurr.Count = 0 then begin
      if wSNext.Count = 0 then
        break;
      wSTemp := wSCurr;
      wSCurr := wSNext;
      wSNext := wSTemp;
      wCurrViolation := p_AllCity[wSCurr.Peek].violationsTo;
    end;

    wCurCity := wSCurr.Pop;
    if p_AllCity[wCurCity].violationsTo < wCurrViolation then
      Continue;

    for wI := 0 to length(p_RConn[wCurCity]) - 1 do begin
      wConnectedCity := p_RConn[wCurCity][wI];
      if p_AllCity[wConnectedCity].violationsTo > p_AllCity[wCurCity].violationsTo then begin
        p_AllCity[wConnectedCity].violationsTo := p_AllCity[wCurCity].violationsTo;
        wSCurr.Push(wConnectedCity);
      end;
    end;
    for wI := 0 to length(p_VConn[wCurCity]) - 1 do begin
      wConnectedCity := p_VConn[wCurCity][wI];
      if p_AllCity[wConnectedCity].violationsTo > p_AllCity[wCurCity].violationsTo + 1 then begin
        p_AllCity[wConnectedCity].violationsTo := p_AllCity[wCurCity].violationsTo + 1;
        wSNext.Push(wConnectedCity);
      end;
    end;
  until false;
  p_AllCity[p_city].minEstimation := wCurrViolation;
  p_AllCity[p_city].maxEstimation := wCurrViolation;

  // wCurrViolation := 0;
  wSCurr.Push(p_city);
  repeat
    if wSCurr.Count = 0 then begin
      if wSNext.Count = 0 then
        break;
      wSTemp := wSCurr;
      wSCurr := wSNext;
      wSNext := wSTemp;
      // wCurrViolation := p_AllCity[wSCurr.Peek].violationsFrom;
    end;

    wCurCity := wSCurr.Pop;
    // if p_AllCity[wCurCity].violationsFrom < wCurrViolation then Continue;

    for wI := 0 to length(p_VConn[wCurCity]) - 1 do begin
      wConnectedCity := p_VConn[wCurCity][wI];
      if p_AllCity[wConnectedCity].violationsFrom > p_AllCity[wCurCity].violationsFrom then begin
        p_AllCity[wConnectedCity].violationsFrom := p_AllCity[wCurCity].violationsFrom;
        wSCurr.Push(wConnectedCity);
      end;
    end;
    for wI := 0 to length(p_RConn[wCurCity]) - 1 do begin
      wConnectedCity := p_RConn[wCurCity][wI];
      if p_AllCity[wConnectedCity].violationsFrom > p_AllCity[wCurCity].violationsFrom + 1 then begin
        p_AllCity[wConnectedCity].violationsFrom := p_AllCity[wCurCity].violationsFrom + 1;
        wSNext.Push(wConnectedCity);
      end;
    end;
  until false;

  wSCurr.Free;
  wSNext.Free;
end;

function SolvAlphaBeta(p_cityCnt: integer; p_RConn: t_connections; p_VConn: t_connections): integer;
var
  wAllCity: array of t_CityDetail;
  wI, wJ, wCurrViolation, wMaxMaxViolation, wMaxMinViolation: integer;
begin
  SetLength(wAllCity, p_cityCnt);
  for wI := 0 to p_cityCnt - 1 do begin
    wAllCity[wI].maxEstimation := BIG;
  end;

  result := -1;
  wI := 0;
  // for wI := 0 to p_cityCnt - 1 do begin
  repeat
    SolvAlphaBetaOne(wAllCity, wI, p_RConn, p_VConn);
    wCurrViolation := wAllCity[wI].maxEstimation;
    wMaxMaxViolation := wCurrViolation;
    wMaxMinViolation := wCurrViolation;
    for wJ := 0 to p_cityCnt - 1 do begin
      if wAllCity[wJ].maxEstimation > wCurrViolation + wAllCity[wJ].violationsFrom then
        wAllCity[wJ].maxEstimation := wCurrViolation + wAllCity[wJ].violationsFrom;
      if wAllCity[wJ].minEstimation < wCurrViolation - wAllCity[wJ].violationsTo then
        wAllCity[wJ].minEstimation := wCurrViolation - wAllCity[wJ].violationsTo;
      if wMaxMaxViolation < wAllCity[wJ].maxEstimation then begin
        wMaxMaxViolation := wAllCity[wJ].maxEstimation;
        wI := wJ;
      end;
      if wMaxMinViolation < wAllCity[wJ].minEstimation then
        wMaxMinViolation := wAllCity[wJ].minEstimation;
    end;
    if wMaxMinViolation = wMaxMaxViolation then begin
      result := wMaxMinViolation;
      break;
    end;
  until false;
end;

function SolvFloyd(p_cityCnt: integer; p_roadCnt: integer; p_roads: t_roads): integer;
var
  wK, wI, wJ, wFrom, wTo: integer;
  wDistance: array of array of integer;
begin
  SetLength(wDistance, p_cityCnt);
  for wI := 0 to p_cityCnt - 1 do begin
    SetLength(wDistance[wI], p_cityCnt);
    for wJ := 0 to p_cityCnt - 1 do begin
      wDistance[wI][wJ] := BIG;
    end;
    wDistance[wI][wI] := 0;
  end;

  for wI := 0 to p_roadCnt - 1 do begin
    wFrom := p_roads[wI][0] - 1;
    wTo := p_roads[wI][1] - 1;
    wDistance[wFrom][wTo] := 0;
    if wDistance[wTo][wFrom] > 1 then
      wDistance[wTo][wFrom] := 1;
  end;

  for wK := 0 to p_cityCnt - 1 do
    for wI := 0 to p_cityCnt - 1 do
      for wJ := 0 to p_cityCnt - 1 do begin
        if wDistance[wI][wJ] > wDistance[wI][wK] + wDistance[wK][wJ] then
          wDistance[wI][wJ] := wDistance[wI][wK] + wDistance[wK][wJ]
      end;

  result := 0;
  for wI := 0 to p_cityCnt - 1 do
    for wJ := 0 to p_cityCnt - 1 do begin
      if result < wDistance[wI][wJ] then
        result := wDistance[wI][wJ];
    end;
end;

end.

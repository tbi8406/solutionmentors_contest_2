program Main;

{$APPTYPE CONSOLE}
{$R *.res}

uses
  System.SysUtils,
  solver in 'solver.pas';

var
  wI, wCityCnt, wRoadCnt, wRoadFrom, wRoadTo, result: integer;
  roads: t_roads;

begin
  read(wCityCnt);
  readln(wRoadCnt);
  setlength(roads, wRoadCnt);

  for wI := 0 to wRoadCnt - 1 do begin
    read(wRoadFrom);
    readln(wRoadTo);
    roads[wI][0] := wRoadFrom;
    roads[wI][1] := wRoadTo;
  end;

  result := Solv(wCityCnt, wRoadCnt, roads, DIJKSTRA);
  writeln(result);
  readln;

end.

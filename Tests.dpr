program Tests;

{$APPTYPE CONSOLE}
{$R *.res}

uses
  System.SysUtils, Windows,
  solver in 'solver.pas';

type
  Input = class
  strict private
    FCityCnt, FRoadCnt, FCurrRoad: integer;
    FRoads: t_roads;
  public
    procedure SetCitiesAndRoads(p_citiCnt: integer; p_roadCnt: integer);
    procedure AddRoad(p_from: integer; p_to: integer);
    property cityCnt: integer read FCityCnt;
    property roadCnt: integer read FRoadCnt;
    property roads: t_roads read FRoads;
  end;

  //The random number generator is designed to be ported to another programming language if desired
  CustomRandom = class
  strict private
    class var
      FRnd : Integer;
  public
    class procedure init(p_val: Integer);
    class function get(p_max: Integer): Integer;
  end;

var
  wInput: Input;

function GetNanoS(): Int64;
begin
  if not QueryPerformanceCounter(Result) then
    Result := 0;
end;
  
procedure Check(p_TestName: string; p_input: Input; p_result: integer);

  procedure CheckSpecific(p_input: Input; p_result: integer; p_algorithm: string);
  var
    wTime: Int64;
    wResult: integer;
    wI: integer;
  begin
    wTime := GetNanoS();
    wResult := Solv(p_input.cityCnt, p_input.roadCnt, p_input.roads, p_algorithm);
    wTime := GetNanoS() - wTime;
    write(#9, wTime, ' ');
    if wResult = p_result then
      write('OK')
    else
      write('failed');
  end;

begin
  write(p_TestName + ':');
  if Length(p_TestName) < 15 then begin
    write(#9);
  end;

  CheckSpecific(p_input, p_result, DIJKSTRA);
  CheckSpecific(p_input, p_result, ALPHA_BETA);
  CheckSpecific(p_input, p_result, FLOYD);
  writeln;
end;

{ Input }

procedure Input.SetCitiesAndRoads(p_citiCnt: integer; p_roadCnt: integer);
begin
  FCityCnt := p_citiCnt;
  FRoadCnt := p_roadCnt;
  SetLength(FRoads, FRoadCnt);
  FCurrRoad := 0;
end;

procedure Input.AddRoad(p_from: integer; p_to: integer);
begin
  FRoads[FCurrRoad][0] := p_from;
  FRoads[FCurrRoad][1] := p_to;
  inc(FCurrRoad);
end;

{ CustomRandom }

const
  MAX_RND = 1 shl 16+1;
  MULTIPLIER_RND = 2087;

class procedure CustomRandom.init(p_val: Integer);
begin
  FRnd := p_val mod MAX_RND;
  FRnd := FRnd * MULTIPLIER_RND mod MAX_RND;
end;


class function CustomRandom.get(p_max: Integer): Integer;
begin
  Result := FRnd mod p_max;
  FRnd := FRnd * MULTIPLIER_RND mod MAX_RND;
end;

{dpr}

procedure Test1FromTask();
begin
  wInput.SetCitiesAndRoads(3, 2);
  wInput.AddRoad(1, 2);
  wInput.AddRoad(1, 3);
  Check('Test1FromTask', wInput, 1);
end;

procedure Test2FromTask();
begin
  wInput.SetCitiesAndRoads(6, 7);
  wInput.AddRoad(1, 2);
  wInput.AddRoad(3, 2);
  wInput.AddRoad(3, 4);
  wInput.AddRoad(5, 4);
  wInput.AddRoad(6, 2);
  wInput.AddRoad(6, 4);
  wInput.AddRoad(5, 2);
  Check('Test2FromTask', wInput, 2);
end;

procedure TestLinear();
var
  wI: integer;
begin
  wInput.SetCitiesAndRoads(100, 99);
  for wI := 1 to 99 do
    wInput.AddRoad(wI, wI + 1);
  Check('TestLinear', wInput, 99);
end;

procedure TestCircle();
var
  wI: integer;
begin
  wInput.SetCitiesAndRoads(100, 100);
  for wI := 1 to 99 do
    wInput.AddRoad(wI, wI + 1);
  wInput.AddRoad(100, 1);
  Check('TestCircle', wInput, 0);
end;

procedure TestShortAndLongRoad();
var
  wI: integer;
begin
  wInput.SetCitiesAndRoads(100, 100);
  for wI := 1 to 99 do
    wInput.AddRoad(wI, wI + 1);
  wInput.AddRoad(1, 100);
  Check('TestShortAndLongRoad', wInput, 1);
end;

procedure TestMultyTree();
begin
  wInput.SetCitiesAndRoads(10, 11);
  wInput.AddRoad(1, 2);
  wInput.AddRoad(2, 3);
  wInput.AddRoad(4, 5);
  wInput.AddRoad(5, 6);
  wInput.AddRoad(7, 8);
  wInput.AddRoad(8, 9);
  wInput.AddRoad(9, 10);

  wInput.AddRoad(1, 5);
  wInput.AddRoad(7, 5);
  wInput.AddRoad(5, 3);
  wInput.AddRoad(5, 9);
  Check('TestMultyTree', wInput, 3);
end;

procedure TestCycles();
const
  CYCLE_CTN = 17;
var
  wI: integer;
begin
  wInput.SetCitiesAndRoads(CYCLE_CTN * 3, CYCLE_CTN * 4 - 1);
  for wI := 0 to CYCLE_CTN - 1 do begin
    if (wI > 0) then
      wInput.AddRoad(wI * 3, wI * 3 + 1);
    wInput.AddRoad(wI * 3 + 1, wI * 3 + 2);
    wInput.AddRoad(wI * 3 + 2, wI * 3 + 3);
    wInput.AddRoad(wI * 3 + 3, wI * 3 + 1);
  end;
  Check('TestCycles', wInput, CYCLE_CTN - 1);
end;

procedure TestCyclesToItsef();
const
  CYCLE_CTN = 17;
var
  wI: integer;
begin
  wInput.SetCitiesAndRoads(CYCLE_CTN * 3, CYCLE_CTN * 6 - 1);
  for wI := 0 to CYCLE_CTN - 1 do begin
    if (wI > 0) then
      wInput.AddRoad(wI * 3, wI * 3 + 1);
    wInput.AddRoad(wI * 3 + 1, wI * 3 + 2);
    wInput.AddRoad(wI * 3 + 2, wI * 3 + 2);
    wInput.AddRoad(wI * 3 + 2, wI * 3 + 3);
    wInput.AddRoad(wI * 3 + 2, wI * 3 + 3);
    wInput.AddRoad(wI * 3 + 3, wI * 3 + 1);
  end;
  Check('TestCyclesToItsef', wInput, CYCLE_CTN - 1);
end;

procedure GraphWikipedia();
begin
  wInput.SetCitiesAndRoads(8, 9);
  wInput.AddRoad(1, 4);
  wInput.AddRoad(1, 5);
  wInput.AddRoad(2, 4);
  wInput.AddRoad(3, 5);
  wInput.AddRoad(3, 8);
  wInput.AddRoad(4, 6);
  wInput.AddRoad(4, 7);
  wInput.AddRoad(4, 8);
  wInput.AddRoad(5, 7);
  Check('GraphWikipedia', wInput, 2);
end;

procedure RandomTest(p_CityCnt: Integer; p_RoadCnt: Integer; p_Salt: Integer);
var
  wFrom, wTo, wResult, wAlgResult: Integer;
  wTime: Int64;
begin
  write('RandomTest(', p_CityCnt, ',', p_RoadCnt, '): '#9);

  CustomRandom.init((p_CityCnt*2027+p_RoadCnt)*2027+p_Salt);
  wInput.SetCitiesAndRoads(p_CityCnt, p_RoadCnt);
  while p_RoadCnt>0 do begin
    wFrom := CustomRandom.get(p_CityCnt) + 1;
    wTo := CustomRandom.get(p_CityCnt) + 1;
    if CustomRandom.get(3)=0 then begin
      wInput.AddRoad(wFrom, wTo);
      Dec(p_RoadCnt);
      if p_RoadCnt = 0 then Break;
      wInput.AddRoad(wTo, wFrom);
    end
    else begin
      wInput.AddRoad(wFrom, wTo);
      Dec(p_RoadCnt);
    end;
  end;

  wTime := GetNanoS();
  wAlgResult := Solv(wInput.cityCnt, wInput.roadCnt, wInput.roads, DIJKSTRA);
  wTime := GetNanoS() - wTime;
  wResult := wAlgResult;
  write(wTime, #9);

  wTime := GetNanoS();
  wAlgResult := Solv(wInput.cityCnt, wInput.roadCnt, wInput.roads, ALPHA_BETA);
  wTime := GetNanoS() - wTime;
  if wResult <> wAlgResult then
    wResult := -1;
  write(wTime, #9);

  wTime := GetNanoS();
  wAlgResult := Solv(wInput.cityCnt, wInput.roadCnt, wInput.roads, FLOYD);
  wTime := GetNanoS() - wTime;
  if wResult <> wAlgResult then
    wResult := -1;
  write(wTime, #9);

  if wResult >= 0 then
      writeln('OK, ', wResult)
    else
      writeln('failed');
end;

begin
  wInput := Input.Create;
  Test1FromTask();
  Test2FromTask();
  TestLinear();
  TestCircle();
  TestShortAndLongRoad();
  TestMultyTree();
  TestCycles();
  TestCyclesToItsef();
  GraphWikipedia();

  writeln;
  RandomTest(3,3,0);
  RandomTest(5,6,3);
  RandomTest(20,50,3);
  RandomTest(20,40,5);
  RandomTest(75,199,1);
  readln;

end.
